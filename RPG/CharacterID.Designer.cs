﻿namespace RPG
{
    partial class CharacterID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_name = new System.Windows.Forms.Label();
            this.pBox_Avatar = new System.Windows.Forms.PictureBox();
            this.btn_main = new System.Windows.Forms.Button();
            this.lbl_Gender = new System.Windows.Forms.Label();
            this.lbl_Class = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Avatar)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(36, 16);
            this.lbl_name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(85, 29);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "label1";
            // 
            // pBox_Avatar
            // 
            this.pBox_Avatar.Location = new System.Drawing.Point(210, 71);
            this.pBox_Avatar.Margin = new System.Windows.Forms.Padding(4);
            this.pBox_Avatar.Name = "pBox_Avatar";
            this.pBox_Avatar.Size = new System.Drawing.Size(133, 62);
            this.pBox_Avatar.TabIndex = 1;
            this.pBox_Avatar.TabStop = false;
            // 
            // btn_main
            // 
            this.btn_main.Location = new System.Drawing.Point(41, 241);
            this.btn_main.Margin = new System.Windows.Forms.Padding(4);
            this.btn_main.Name = "btn_main";
            this.btn_main.Size = new System.Drawing.Size(100, 28);
            this.btn_main.TabIndex = 2;
            this.btn_main.Text = "Main Menu";
            this.btn_main.UseVisualStyleBackColor = true;
            this.btn_main.Click += new System.EventHandler(this.btn_main_Click);
            // 
            // lbl_Gender
            // 
            this.lbl_Gender.AutoSize = true;
            this.lbl_Gender.Location = new System.Drawing.Point(41, 71);
            this.lbl_Gender.Name = "lbl_Gender";
            this.lbl_Gender.Size = new System.Drawing.Size(46, 17);
            this.lbl_Gender.TabIndex = 3;
            this.lbl_Gender.Text = "label1";
            // 
            // lbl_Class
            // 
            this.lbl_Class.AutoSize = true;
            this.lbl_Class.Location = new System.Drawing.Point(41, 105);
            this.lbl_Class.Name = "lbl_Class";
            this.lbl_Class.Size = new System.Drawing.Size(46, 17);
            this.lbl_Class.TabIndex = 4;
            this.lbl_Class.Text = "label1";
            // 
            // CharacterID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 336);
            this.Controls.Add(this.lbl_Class);
            this.Controls.Add(this.lbl_Gender);
            this.Controls.Add(this.btn_main);
            this.Controls.Add(this.pBox_Avatar);
            this.Controls.Add(this.lbl_name);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CharacterID";
            this.Text = "CharacterID";
            this.Load += new System.EventHandler(this.CharacterID_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pBox_Avatar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.PictureBox pBox_Avatar;
        private System.Windows.Forms.Button btn_main;
        private System.Windows.Forms.Label lbl_Gender;
        private System.Windows.Forms.Label lbl_Class;
    }
}