﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Classes
{
    public class Mage : Entity
    {
        // Add a random Generator
        Random rand = new Random();

        // Parameterless Constructor
        public Mage()
            : base()
        {
        }
        public Mage(string name, EntityGender gender)
            : base()
        {
            // TODO: Add some formulas for generating stats
            // Base those formulas on the Class
            Name = name; // Set Property of Name to the name given
            Gender = gender;
            _wisdomModifier = rand.Next(3, 8); // Add wisdom for Mage
        }
    }
}
