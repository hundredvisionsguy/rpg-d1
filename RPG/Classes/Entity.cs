﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml; // All using statements from here down were added
using System.Xml.Serialization;
using System.Xml.Xsl;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging; // I had to add a reference to the PresentationCore to get this directive
using System.ComponentModel;

namespace RPG.Classes
{
    public enum EntityGender { Male, Female, Unknown }
    public enum EntityClass { Mage, Rogue, Cleric, Paladin, Lumberjack, Unknown }

    public abstract class Entity
    {
        #region Field Region
        protected string entityType;
        protected EntityGender _gender;
        protected EntityClass _characterClass;
        protected string _name;
        protected int _strength;
        protected int _dexterity;
        protected int _wisdom;
        protected int _health;
        protected int _strengthModifier;
        protected int _dexterityModifier;
        protected int _wisdomModifier;
        protected int _healthModifier;
        protected Image _avatar;
        protected string _avatarString;
        
        #endregion
        #region Property Region
        public EntityGender Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public EntityClass CharacterClass
        {
            get { return _characterClass; }
            set { _characterClass = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int Strength
        {
            get { return _strength + _strengthModifier; }
            set { _strength = value; }
        }
        public int Dexterity
        {
            get { return _dexterity + _dexterityModifier; }
            set { _dexterity = value; }
        }
        public int Wisdom
        {
            get { return _wisdom + _wisdomModifier; }
            set { _wisdom = value; }
        }
        public int Health
        {
            get { return _health + _healthModifier; } 
            set { _health = value; }
        }
        [XmlIgnore]
        public Image Avatar
        {
            get { return _avatar; }
            set { 
                _avatar = value;
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(Bitmap));
                _avatarString = Convert.ToBase64String((byte[])converter.ConvertTo(_avatar, typeof(byte[])));
            }
        }
        [XmlElement("AvatarData")]
        public String AvatarString
        {
            get 
            {
                return _avatarString;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("path");
                try
                {
                    _avatarString = value;
                }
                catch (IOException ioe)
                {
                    MessageBox.Show(ioe.ToString());
                }
            }
        }
        #endregion
        #region Constructor Region
        public Entity()
        {
            // Set initial property values
            Name = "";
            Gender = EntityGender.Unknown;
            CharacterClass = EntityClass.Unknown;
            Strength = 0;
            Dexterity = 0;
            Wisdom = 0;
            Health = 0;
            // Avatar = RPG.Properties.Resources.avatar;
        }
        #endregion
        #region Method Region
        public override string ToString()
        {
 	            return base.ToString();
        }
        private void avatarFromString(string imageString)
        {
            if (imageString == null)
                throw new ArgumentNullException("imageString");
            byte[] array = Convert.FromBase64String(imageString);
            _avatar = Image.FromStream(new MemoryStream(array));
        }
        #endregion
        #region Virtual Method region
        #endregion

    }
}
