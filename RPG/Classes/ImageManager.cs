﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace RPG.Classes
{
    class ImageManager
    {
        #region Field Region
        
        #endregion
        #region Property Region

        #endregion
        #region Methods Region
        public static Image StringToImage(string imageString)
        {
            if (imageString == null)
                throw new ArgumentNullException("imageString");
            byte[] array = Convert.FromBase64String(imageString);
            Image img = Image.FromStream(new MemoryStream(array));
            return img;
        }
        public static String ImageToString(string path)
        {
            if (path == null)
                throw new ArgumentNullException("path");
            Image im = Image.FromFile(path);
            MemoryStream ms = new MemoryStream();
            im.Save(ms, im.RawFormat);
            byte[] array = ms.ToArray();
            return Convert.ToBase64String(array);
        }
        #endregion
    }
}
