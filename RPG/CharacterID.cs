﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; // Added for XML Serialization
using System.Xml; // Added for XML Serialization
using System.Xml.Serialization; // Added for XML Serialization
using RPG.Classes; // Added for RPG classes
using RPG.Properties;

namespace RPG
{
    public partial class CharacterID : Form
    {

        // Initialize Controls
        //private Button btn_main = new Button();
        //private PictureBox pbx_avatar = new PictureBox();
        

        public CharacterID()
        {
            InitializeComponent();
        }

        private void btn_main_Click(object sender, EventArgs e)
        {
            
            // return to home menu
            Frm_Menu main_menu = new Frm_Menu();
            main_menu.Show();
            this.Close();
        }
        private void CharacterID_Activated(object sender, System.EventArgs e)
        {
            
        }
        // File & folder Properties
        private static string SettingsFile
        {
            get
            {
                return Path.Combine(SettingsFolder, "player.xml");
            }
        }
        private static string SettingsFolder
        {
            get
            {
                // Build our folder path
                string folder = Environment.GetFolderPath(
                    Environment.SpecialFolder.ApplicationData);
                // add a subfolder
                folder = Path.Combine(folder, "RPG Project");
                folder = Path.Combine(folder, "Character Settings");
                // Make sure the folder exists
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);
                return folder;
            }
        }
        private static Player LoadPlayer()
        {
            if (!File.Exists(SettingsFile))
                return DefaultPlayer;

            using (Stream stream = File.OpenRead(SettingsFile))
            {
                XmlSerializer ser = new XmlSerializer(typeof(Player));
                return (Player)ser.Deserialize(stream);
            }

        }
        private static Player DefaultPlayer
        {
            get
            {
                EntityGender eGen = new EntityGender();
                EntityClass eClass = new EntityClass();
                Player defPlayer = new Player("Fred", eGen, eClass);
                return defPlayer;
            }
        }

        private void CharacterID_Load(object sender, EventArgs e)
        {
            Player p1 = new Player();
            p1 = LoadPlayer();
            Image avatar = ImageManager.StringToImage(p1.AvatarString);
            lbl_name.Text = p1.Name;
            lbl_Gender.Text = p1.Gender.ToString();
            lbl_Class.Text = p1.CharacterClass.ToString(); 
            pBox_Avatar.Image = avatar;
            
        }
    }
}
