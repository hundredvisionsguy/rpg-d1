﻿namespace RPG
{
    partial class FormCharacterCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCharacterCreator));
            this.Lbl_CharacterName = new System.Windows.Forms.Label();
            this.Txt_CharacterName = new System.Windows.Forms.TextBox();
            this.Gbox_Gender = new System.Windows.Forms.GroupBox();
            this.Rdo_GenderFemale = new System.Windows.Forms.RadioButton();
            this.Rdo_GenderMale = new System.Windows.Forms.RadioButton();
            this.Cbo_CharacterClass = new System.Windows.Forms.ComboBox();
            this.Btn_SaveCharacter = new System.Windows.Forms.Button();
            this.openFileD_Avatar = new System.Windows.Forms.OpenFileDialog();
            this.picBox_Avatar = new System.Windows.Forms.PictureBox();
            this.Txt_ImagePath = new System.Windows.Forms.TextBox();
            this.Btn_UploadImage = new System.Windows.Forms.Button();
            this.Btn_SetAvatar = new System.Windows.Forms.Button();
            this.Gbox_Gender.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Avatar)).BeginInit();
            this.SuspendLayout();
            // 
            // Lbl_CharacterName
            // 
            this.Lbl_CharacterName.AutoSize = true;
            this.Lbl_CharacterName.Location = new System.Drawing.Point(17, 16);
            this.Lbl_CharacterName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_CharacterName.Name = "Lbl_CharacterName";
            this.Lbl_CharacterName.Size = new System.Drawing.Size(45, 17);
            this.Lbl_CharacterName.TabIndex = 0;
            this.Lbl_CharacterName.Text = "Name";
            this.Lbl_CharacterName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Txt_CharacterName
            // 
            this.Txt_CharacterName.Location = new System.Drawing.Point(73, 16);
            this.Txt_CharacterName.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_CharacterName.Name = "Txt_CharacterName";
            this.Txt_CharacterName.Size = new System.Drawing.Size(132, 22);
            this.Txt_CharacterName.TabIndex = 1;
            // 
            // Gbox_Gender
            // 
            this.Gbox_Gender.Controls.Add(this.Rdo_GenderFemale);
            this.Gbox_Gender.Controls.Add(this.Rdo_GenderMale);
            this.Gbox_Gender.Location = new System.Drawing.Point(21, 63);
            this.Gbox_Gender.Margin = new System.Windows.Forms.Padding(4);
            this.Gbox_Gender.Name = "Gbox_Gender";
            this.Gbox_Gender.Padding = new System.Windows.Forms.Padding(4);
            this.Gbox_Gender.Size = new System.Drawing.Size(267, 48);
            this.Gbox_Gender.TabIndex = 2;
            this.Gbox_Gender.TabStop = false;
            this.Gbox_Gender.Text = "Gender";
            // 
            // Rdo_GenderFemale
            // 
            this.Rdo_GenderFemale.AutoSize = true;
            this.Rdo_GenderFemale.Location = new System.Drawing.Point(83, 20);
            this.Rdo_GenderFemale.Margin = new System.Windows.Forms.Padding(4);
            this.Rdo_GenderFemale.Name = "Rdo_GenderFemale";
            this.Rdo_GenderFemale.Size = new System.Drawing.Size(75, 21);
            this.Rdo_GenderFemale.TabIndex = 1;
            this.Rdo_GenderFemale.TabStop = true;
            this.Rdo_GenderFemale.Text = "Female";
            this.Rdo_GenderFemale.UseVisualStyleBackColor = true;
            // 
            // Rdo_GenderMale
            // 
            this.Rdo_GenderMale.AutoSize = true;
            this.Rdo_GenderMale.Location = new System.Drawing.Point(9, 20);
            this.Rdo_GenderMale.Margin = new System.Windows.Forms.Padding(4);
            this.Rdo_GenderMale.Name = "Rdo_GenderMale";
            this.Rdo_GenderMale.Size = new System.Drawing.Size(59, 21);
            this.Rdo_GenderMale.TabIndex = 0;
            this.Rdo_GenderMale.TabStop = true;
            this.Rdo_GenderMale.Text = "Male";
            this.Rdo_GenderMale.UseVisualStyleBackColor = true;
            // 
            // Cbo_CharacterClass
            // 
            this.Cbo_CharacterClass.FormattingEnabled = true;
            this.Cbo_CharacterClass.Items.AddRange(new object[] {
            "Mage",
            "Rogue",
            "Cleric",
            "Paladin",
            "Lumberjack"});
            this.Cbo_CharacterClass.Location = new System.Drawing.Point(21, 212);
            this.Cbo_CharacterClass.Margin = new System.Windows.Forms.Padding(4);
            this.Cbo_CharacterClass.Name = "Cbo_CharacterClass";
            this.Cbo_CharacterClass.Size = new System.Drawing.Size(171, 24);
            this.Cbo_CharacterClass.TabIndex = 5;
            this.Cbo_CharacterClass.SelectedIndexChanged += new System.EventHandler(this.Cbo_CharacterClass_SelectedIndexChanged);
            // 
            // Btn_SaveCharacter
            // 
            this.Btn_SaveCharacter.Location = new System.Drawing.Point(21, 321);
            this.Btn_SaveCharacter.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_SaveCharacter.Name = "Btn_SaveCharacter";
            this.Btn_SaveCharacter.Size = new System.Drawing.Size(161, 50);
            this.Btn_SaveCharacter.TabIndex = 6;
            this.Btn_SaveCharacter.Text = "Save Your Character";
            this.Btn_SaveCharacter.UseVisualStyleBackColor = true;
            this.Btn_SaveCharacter.Click += new System.EventHandler(this.Btn_SaveCharacter_Click);
            // 
            // openFileD_Avatar
            // 
            this.openFileD_Avatar.FileName = "openFileDialog1";
            // 
            // picBox_Avatar
            // 
            this.picBox_Avatar.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picBox_Avatar.ErrorImage")));
            this.picBox_Avatar.Image = global::RPG.Properties.Resources.avatar;
            this.picBox_Avatar.InitialImage = ((System.Drawing.Image)(resources.GetObject("picBox_Avatar.InitialImage")));
            this.picBox_Avatar.Location = new System.Drawing.Point(235, 212);
            this.picBox_Avatar.Margin = new System.Windows.Forms.Padding(4);
            this.picBox_Avatar.Name = "picBox_Avatar";
            this.picBox_Avatar.Size = new System.Drawing.Size(185, 159);
            this.picBox_Avatar.TabIndex = 5;
            this.picBox_Avatar.TabStop = false;
            this.picBox_Avatar.Click += new System.EventHandler(this.picBox_Avatar_Click);
            // 
            // Txt_ImagePath
            // 
            this.Txt_ImagePath.Location = new System.Drawing.Point(21, 137);
            this.Txt_ImagePath.Name = "Txt_ImagePath";
            this.Txt_ImagePath.Size = new System.Drawing.Size(241, 22);
            this.Txt_ImagePath.TabIndex = 7;
            // 
            // Btn_UploadImage
            // 
            this.Btn_UploadImage.Location = new System.Drawing.Point(268, 136);
            this.Btn_UploadImage.Name = "Btn_UploadImage";
            this.Btn_UploadImage.Size = new System.Drawing.Size(108, 23);
            this.Btn_UploadImage.TabIndex = 3;
            this.Btn_UploadImage.Text = "Load Avatar";
            this.Btn_UploadImage.UseVisualStyleBackColor = true;
            this.Btn_UploadImage.Click += new System.EventHandler(this.Btn_UploadImage_Click);
            // 
            // Btn_SetAvatar
            // 
            this.Btn_SetAvatar.Location = new System.Drawing.Point(268, 166);
            this.Btn_SetAvatar.Name = "Btn_SetAvatar";
            this.Btn_SetAvatar.Size = new System.Drawing.Size(107, 23);
            this.Btn_SetAvatar.TabIndex = 4;
            this.Btn_SetAvatar.Text = "Set Avatar";
            this.Btn_SetAvatar.UseVisualStyleBackColor = true;
            this.Btn_SetAvatar.Click += new System.EventHandler(this.Btn_SetAvatar_Click);
            // 
            // FormCharacterCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 415);
            this.Controls.Add(this.Btn_SetAvatar);
            this.Controls.Add(this.Btn_UploadImage);
            this.Controls.Add(this.Txt_ImagePath);
            this.Controls.Add(this.picBox_Avatar);
            this.Controls.Add(this.Btn_SaveCharacter);
            this.Controls.Add(this.Cbo_CharacterClass);
            this.Controls.Add(this.Gbox_Gender);
            this.Controls.Add(this.Txt_CharacterName);
            this.Controls.Add(this.Lbl_CharacterName);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormCharacterCreator";
            this.Text = "Create Your Character";
            this.Load += new System.EventHandler(this.FormCharacterCreator_Load);
            this.Gbox_Gender.ResumeLayout(false);
            this.Gbox_Gender.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Avatar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Lbl_CharacterName;
        private System.Windows.Forms.TextBox Txt_CharacterName;
        private System.Windows.Forms.GroupBox Gbox_Gender;
        private System.Windows.Forms.RadioButton Rdo_GenderFemale;
        private System.Windows.Forms.RadioButton Rdo_GenderMale;
        private System.Windows.Forms.ComboBox Cbo_CharacterClass;
        private System.Windows.Forms.Button Btn_SaveCharacter;
        private System.Windows.Forms.OpenFileDialog openFileD_Avatar;
        private System.Windows.Forms.PictureBox picBox_Avatar;
        private System.Windows.Forms.TextBox Txt_ImagePath;
        private System.Windows.Forms.Button Btn_UploadImage;
        private System.Windows.Forms.Button Btn_SetAvatar;
    }
}