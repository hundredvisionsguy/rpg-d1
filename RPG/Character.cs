﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG
{
    class Character
    {
        // Fields
        string _name, _gender, _class;
        int _strength, _accuracy, _defense, _speed, _health;
        public Character()
        {
            _speed = 0;
            _strength = 0;
            _accuracy = 0;
            _defense = 0;
            _health = 0;
        }
            public Character(string name)
        {
            _name = name;
        }
        
        public string Name {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }
        public string Class
        {
            get
            {
                return _class;
            }
            set
            {
                _class = value;
            }
        }
    }
}
