﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using RPG.Classes;

namespace RPG
{
    public partial class FormCharacterCreator: Form
    {
        
       
        public FormCharacterCreator()
        {
            InitializeComponent();
        }

        private void FormCharacterCreator_Load(object sender, EventArgs e)
        {
            Button btn_Carl2;
            btn_Carl2 = new Button();
        }

        private void Btn_SaveCharacter_Click(object sender, EventArgs e)
        {
            // Capture info from the form
            // Make sure the fields are not empty
            // Check for name
            string name;

            if (String.IsNullOrEmpty(Txt_CharacterName.Text) ||
                Txt_CharacterName.Text[0] == ' ')
            {
                MessageBox.Show("You must name your character. " +
                    "Note also: names cannot begin with a space");
                return;
            }
            name = this.Txt_CharacterName.Text;

            // Check the gender
            EntityGender eGender = new EntityGender();
            eGender = EntityGender.Unknown;
            if (this.Rdo_GenderMale.Checked)
                eGender = EntityGender.Male;
            else if (this.Rdo_GenderFemale.Checked)
                eGender = EntityGender.Female;
            else
            {
                MessageBox.Show("You must select a gender.");
                return;
            }

            // Check the class
            EntityClass eClass;
            eClass = EntityClass.Unknown;
            if (this.Cbo_CharacterClass.Text == "Mage")
                eClass = EntityClass.Mage;
            else if (this.Cbo_CharacterClass.Text == "Rogue")
                eClass = EntityClass.Rogue;
            else if (this.Cbo_CharacterClass.Text == "Cleric")
                eClass = EntityClass.Cleric;
            else if (this.Cbo_CharacterClass.Text == "Paladin")
                eClass = EntityClass.Paladin;
            else if (this.Cbo_CharacterClass.Text == "Lumberjack")
                eClass = EntityClass.Lumberjack;
            else
            {
                MessageBox.Show("You must choose a class for your character");
                return;
            }

            // Create our object
            Player player1 = new Player(name, eGender, eClass);
            player1.Avatar = picBox_Avatar.Image;
            
            string output;
            output = String.Format("You created a new character.\n" +
                "Your name is: {0}\nYour gender is: {1}\n" +
                "Your class is: {2}.", player1.Name,
                player1.Gender.ToString(),
                player1.CharacterClass.ToString());
            MessageBox.Show(output, "Success");

            // Serialize our player
            StoreCharacter(player1);
            // return to home menu
            CharacterID charId = new CharacterID();
            charId.Show();
            this.Close();
        }
        private void StoreCharacter(Player player)
        {
            using (Stream stream = File.Create(SettingsFile))
            {
                XmlSerializer ser = new XmlSerializer(player.GetType());
                ser.Serialize(stream, player);
            }
        }
        // File & folder Properties
        private static string SettingsFile
        {
            get
            {
                return Path.Combine(SettingsFolder, "player.xml");
            }
        }
        private static string SettingsFolder
        {
            get
            {
                // Build our folder path
                string folder = Environment.GetFolderPath(
                    Environment.SpecialFolder.ApplicationData);
                // add a subfolder
                folder = Path.Combine(folder, "RPG Project");
                folder = Path.Combine(folder, "Character Settings");
                // Make sure the folder exists
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);
                return folder;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void Btn_UploadImage_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    Txt_ImagePath.Text = ofd.FileName;
                }
            }
        }

        private void Btn_SetAvatar_Click(object sender, EventArgs e)
        {
            try
            {
                Image AvImg = Image.FromFile(Txt_ImagePath.Text);
                picBox_Avatar.Image = AvImg;
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void picBox_Avatar_Click(object sender, EventArgs e)
        {

        }

        private void Cbo_CharacterClass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
