﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPG
{
    public partial class Frm_Menu : Form
    {
        public Frm_Menu()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Btn_CreateCharacter_Click(object sender, EventArgs e)
        {
            FormCharacterCreator charcreator = new FormCharacterCreator();
            charcreator.Show();
            this.Hide();
        }
    }
}
