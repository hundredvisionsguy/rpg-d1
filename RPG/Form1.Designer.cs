﻿namespace RPG
{
    partial class Frm_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Menu));
            this.Lbl_GameTitle = new System.Windows.Forms.Label();
            this.Btn_CreateCharacter = new System.Windows.Forms.Button();
            this.BtnLoadGame = new System.Windows.Forms.Button();
            this.Btn_HighScores = new System.Windows.Forms.Button();
            this.Btn_Credits = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Lbl_GameTitle
            // 
            this.Lbl_GameTitle.AutoSize = true;
            this.Lbl_GameTitle.Font = new System.Drawing.Font("Pericles", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_GameTitle.Location = new System.Drawing.Point(13, 13);
            this.Lbl_GameTitle.Name = "Lbl_GameTitle";
            this.Lbl_GameTitle.Size = new System.Drawing.Size(333, 27);
            this.Lbl_GameTitle.TabIndex = 0;
            this.Lbl_GameTitle.Text = "RPG Game Title Goes Here";
            // 
            // Btn_CreateCharacter
            // 
            this.Btn_CreateCharacter.Location = new System.Drawing.Point(18, 178);
            this.Btn_CreateCharacter.Name = "Btn_CreateCharacter";
            this.Btn_CreateCharacter.Size = new System.Drawing.Size(102, 35);
            this.Btn_CreateCharacter.TabIndex = 1;
            this.Btn_CreateCharacter.Text = "New Game";
            this.Btn_CreateCharacter.UseVisualStyleBackColor = true;
            this.Btn_CreateCharacter.Click += new System.EventHandler(this.Btn_CreateCharacter_Click);
            // 
            // BtnLoadGame
            // 
            this.BtnLoadGame.Location = new System.Drawing.Point(126, 178);
            this.BtnLoadGame.Name = "BtnLoadGame";
            this.BtnLoadGame.Size = new System.Drawing.Size(102, 35);
            this.BtnLoadGame.TabIndex = 2;
            this.BtnLoadGame.Text = "Load Game";
            this.BtnLoadGame.UseVisualStyleBackColor = true;
            // 
            // Btn_HighScores
            // 
            this.Btn_HighScores.Location = new System.Drawing.Point(126, 230);
            this.Btn_HighScores.Name = "Btn_HighScores";
            this.Btn_HighScores.Size = new System.Drawing.Size(102, 35);
            this.Btn_HighScores.TabIndex = 4;
            this.Btn_HighScores.Text = "High Scores";
            this.Btn_HighScores.UseVisualStyleBackColor = true;
            // 
            // Btn_Credits
            // 
            this.Btn_Credits.Location = new System.Drawing.Point(234, 230);
            this.Btn_Credits.Name = "Btn_Credits";
            this.Btn_Credits.Size = new System.Drawing.Size(102, 35);
            this.Btn_Credits.TabIndex = 5;
            this.Btn_Credits.Text = "Credits";
            this.Btn_Credits.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::RPG.Properties.Resources.RPG_Background1;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(448, 328);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Frm_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 328);
            this.Controls.Add(this.Btn_Credits);
            this.Controls.Add(this.Btn_HighScores);
            this.Controls.Add(this.BtnLoadGame);
            this.Controls.Add(this.Btn_CreateCharacter);
            this.Controls.Add(this.Lbl_GameTitle);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_Menu";
            this.Text = "RPG Main Menu";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Lbl_GameTitle;
        private System.Windows.Forms.Button Btn_CreateCharacter;
        private System.Windows.Forms.Button BtnLoadGame;
        private System.Windows.Forms.Button Btn_HighScores;
        private System.Windows.Forms.Button Btn_Credits;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

