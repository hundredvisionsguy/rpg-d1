# C# RPG Windows Form Project #

## *A Little History* ##
This is my repository for the C# Windows Role Playing Game. I began the project when I was teaching C# to my Programming 2 students. I wanted to cover both creating Windows forms and learning about Object Oriented Programming. 

We didn't get very far before the end of the school year came up, and unfortunately, I had to abandon the project over the summer. By the next year, I switched to teaching Java, so I've never been able to complete the program.

It's a starter, and anyone is welcome to clone or download this project and extend it. Who knows, maybe I'll pick this up again in the future, but for now, it's just a start.